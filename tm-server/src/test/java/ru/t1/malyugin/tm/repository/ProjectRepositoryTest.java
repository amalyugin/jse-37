package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private static User user1 = new User();

    @NotNull
    private static User user2 = new User();

    @BeforeClass
    public static void initUser() throws Exception {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() throws Exception {
        USER_SERVICE.remove(user1);
        USER_SERVICE.remove(user2);
    }

    @Before
    public void initRepository() throws Exception {
        for (int i = 1; i <= NUMBER_OF_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project N " + i);
            project.setDescription("Desc " + i);
            if (i <= NUMBER_OF_PROJECTS / 2) project.setUserId(user1.getId());
            else project.setUserId(user2.getId());
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() throws Exception {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        PROJECT_LIST.clear();
        TASK_LIST.clear();
    }

    @Test
    public void testClear() throws Exception {
        final int expectedNumberOfEntries = 0;
        PROJECT_REPOSITORY.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testClearForUser() throws Exception {
        final int expectedNumberOfEntries = 0;

        PROJECT_REPOSITORY.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_PROJECTS, PROJECT_REPOSITORY.getSize());

        PROJECT_REPOSITORY.clear(user1.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(user1.getId()));
        Assert.assertNotEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(user2.getId()));
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(PROJECT_LIST.size(), PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        final int userProjectListSize = (int) PROJECT_LIST.stream().filter(p -> user1.getId().equals(p.getUserId())).count();
        Assert.assertEquals(userProjectListSize, PROJECT_REPOSITORY.getSize(user1.getId()));
    }

    @Test
    public void testAddProject() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS + 1;
        @NotNull final Project project = new Project("NEW", "NEW");
        PROJECT_LIST.add(project);
        PROJECT_REPOSITORY.add(project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testAddProjectForUser() throws Exception {
        final int expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(user1.getId()) + 1;
        @NotNull final Project project = new Project();
        PROJECT_REPOSITORY.add(user1.getId(), project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(user1.getId()));
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(user2.getId(), project.getId()));
    }

    @Test
    public void testAddAll() throws Exception {
        int expectedNumberOfEntries = NUMBER_OF_PROJECTS * 2;
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_PROJECTS; i++) {
            projectList.add(new Project());
        }
        PROJECT_REPOSITORY.addAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
    }

    @Test
    public void testSet() throws Exception {
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_PROJECTS; i++) {
            projectList.add(new Project());
        }
        PROJECT_REPOSITORY.set(projectList);
        @NotNull final List<Project> actualProjectList = PROJECT_REPOSITORY.findAll();
        Assert.assertEquals(projectList.size(), actualProjectList.size());
        for (int i = 0; i < projectList.size(); i++)
            Assert.assertEquals(projectList.get(i).getId(), actualProjectList.get(i).getId());
    }

    @Test
    public void testRemove() throws Exception {
        final int initialNumberOfEntries = PROJECT_REPOSITORY.getSize();
        final int expectedNumberOfEntries = initialNumberOfEntries - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_REPOSITORY.remove(project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(project.getId()));
    }

    @Test
    public void testRemoveForUser() throws Exception {
        final int initialNumberOfEntries = PROJECT_REPOSITORY.getSize(user1.getId());
        int expectedNumberOfEntries = initialNumberOfEntries - 1;
        @NotNull final Project project1 = PROJECT_REPOSITORY.findAll(user1.getId()).get(0);
        PROJECT_REPOSITORY.remove(user1.getId(), project1);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(user1.getId()));
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(user1.getId(), project1.getId()));

        expectedNumberOfEntries = PROJECT_REPOSITORY.getSize(user2.getId());
        @NotNull final Project project2 = PROJECT_REPOSITORY.findAll(user2.getId()).get(0);

        PROJECT_REPOSITORY.remove(user1.getId(), project2);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize(user2.getId()));
    }

    @Test
    public void testRemoveAll() throws Exception {
        final int initialNumberOfEntries = PROJECT_REPOSITORY.getSize();
        final int expectedNumberOfEntries = initialNumberOfEntries - PROJECT_LIST.size();
        PROJECT_REPOSITORY.removeAll(PROJECT_LIST);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(PROJECT_LIST.get(0).getId()));
    }

    @Test
    public void testRemoveAllForUser() throws Exception {
        @NotNull final List<Project> firstUserProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .collect(Collectors.toList());
        int expectedNumberOfEntries = PROJECT_REPOSITORY.getSize();
        PROJECT_REPOSITORY.removeAll(user2.getId(), firstUserProjectList);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        expectedNumberOfEntries = PROJECT_REPOSITORY.getSize() - firstUserProjectList.size();
        PROJECT_REPOSITORY.removeAll(user1.getId(), firstUserProjectList);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(firstUserProjectList.get(0).getId()));
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Project> projectList = PROJECT_REPOSITORY.findAll();

        for (int i = 0; i < projectList.size(); i++) {
            Assert.assertEquals(PROJECT_LIST.get(i).getId(), projectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllForUser() throws Exception {
        @NotNull final List<Project> actualProjectList = PROJECT_REPOSITORY.findAll(user1.getId());
        @NotNull final List<Project> expectedProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(actualProjectList.size(), expectedProjectList.size());
        for (int i = 0; i < actualProjectList.size(); i++) {
            Assert.assertEquals(expectedProjectList.get(i).getId(), actualProjectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllWithComparator() {

    }

    @Test
    public void testFindAllWithComparatorForUser() {

    }

    @Test
    public void testFindOneById() throws Exception {
        for (@NotNull final Project project : PROJECT_LIST) {
            Assert.assertEquals(project.getId(), PROJECT_REPOSITORY.findOneById(project.getId()).getId());
        }
        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(id));
    }

    @Test
    public void testFindOneByIdForUser() throws Exception {
        @NotNull final List<Project> userProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .collect(Collectors.toList());

        for (@NotNull final Project project : userProjectList) {
            Assert.assertEquals(project.getId(), PROJECT_REPOSITORY.findOneById(user1.getId(), project.getId()).getId());
            Assert.assertNull(PROJECT_REPOSITORY.findOneById(user2.getId(), project.getId()));
        }

        @NotNull final String id = UNKNOWN_ID;
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(user1.getId(), id));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        @NotNull final List<Project> actualProjectList = PROJECT_REPOSITORY.findAll();
        Assert.assertEquals(PROJECT_LIST.size(), actualProjectList.size());

        for (int i = 0; i < PROJECT_LIST.size(); i++) {
            Assert.assertEquals(PROJECT_LIST.get(i).getId(), PROJECT_REPOSITORY.findOneByIndex(i + 1).getId());
        }
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @NotNull final List<Project> userProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .collect(Collectors.toList());

        for (int i = 0; i < userProjectList.size(); i++) {
            Assert.assertEquals(userProjectList.get(i).getId(), PROJECT_REPOSITORY.findOneByIndex(user1.getId(), i + 1).getId());
            Assert.assertNotEquals(userProjectList.get(i).getId(), PROJECT_REPOSITORY.findOneByIndex(user2.getId(), i + 1).getId());
        }
    }

    @Test
    public void testUpdate() throws Exception {
        for (@NotNull final Project project : PROJECT_LIST) {
            @NotNull final String newName = "NEW PROJECT";
            @NotNull final String newDescription = "NEW PROJECT DESC";
            @NotNull final Status newStatus = Status.COMPLETED;
            project.setName(newName);
            project.setDescription(newDescription);
            project.setStatus(newStatus);
            PROJECT_REPOSITORY.update(project);
            @Nullable final Project newProject = PROJECT_REPOSITORY.findOneById(project.getId());
            Assert.assertNotNull(newProject);
            Assert.assertEquals(newName, newProject.getName());
            Assert.assertEquals(newDescription, newProject.getDescription());
            Assert.assertEquals(newStatus, newProject.getStatus());
        }
    }

}