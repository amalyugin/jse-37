package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IRepository;
import ru.t1.malyugin.tm.comparator.CreatedComparator;
import ru.t1.malyugin.tm.comparator.StatusComparator;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return DBConstant.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return DBConstant.COLUMN_STATUS;
        else return DBConstant.COLUMN_NAME;
    }

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row) throws Exception;

    @NotNull
    public abstract M add(@NotNull final M model) throws Exception;

    @NotNull
    @Override
    public Collection<M> addAll(@NotNull final Collection<M> models) throws Exception {
        @NotNull final List<M> result = new ArrayList<M>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        clear();
        return addAll(models);
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(), DBConstant.COLUMN_ROW_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            else return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s LIMIT ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) resultSet.next();
            return fetch(resultSet);
        }
    }

    @Override
    public void clear() throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @Override
    public void remove(@NotNull final M model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), DBConstant.COLUMN_ROW_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Override
    public void removeAll(@NotNull final Collection<M> models) throws Exception {
        for (@NotNull final M model : models) remove(model);
    }

}