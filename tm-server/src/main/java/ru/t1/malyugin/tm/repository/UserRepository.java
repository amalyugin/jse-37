package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstant.TABLE_USER;
    }

    @NotNull
    @Override
    public User fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConstant.COLUMN_ROW_ID));
        user.setLocked(row.getBoolean(DBConstant.COLUMN_LOCKED));
        user.setLogin(row.getString(DBConstant.COLUMN_LOGIN));
        user.setFirstName(row.getString(DBConstant.COLUMN_FIRST_NAME));
        user.setMiddleName(row.getString(DBConstant.COLUMN_MIDDLE_NAME));
        user.setLastName(row.getString(DBConstant.COLUMN_LAST_NAME));
        user.setPasswordHash(row.getString(DBConstant.COLUMN_PASSWORD_HASH));
        user.setEmail(row.getString(DBConstant.COLUMN_EMAIL));
        user.setRole(Role.valueOf(row.getString(DBConstant.COLUMN_ROLE)));
        return user;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstant.COLUMN_ROW_ID, DBConstant.COLUMN_LOCKED, DBConstant.COLUMN_LOGIN,
                DBConstant.COLUMN_FIRST_NAME, DBConstant.COLUMN_MIDDLE_NAME, DBConstant.COLUMN_LAST_NAME,
                DBConstant.COLUMN_PASSWORD_HASH, DBConstant.COLUMN_EMAIL, DBConstant.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setBoolean(2, user.getLocked());
            statement.setString(3, user.getLogin());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getMiddleName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getPasswordHash());
            statement.setString(8, user.getEmail());
            statement.setString(9, user.getRole().toString());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), DBConstant.COLUMN_LOGIN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            else return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), DBConstant.COLUMN_EMAIL);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            else return fetch(resultSet);
        }
    }

    @Override
    public void update(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstant.COLUMN_LOCKED, DBConstant.COLUMN_FIRST_NAME, DBConstant.COLUMN_MIDDLE_NAME,
                DBConstant.COLUMN_LAST_NAME, DBConstant.COLUMN_PASSWORD_HASH, DBConstant.COLUMN_ROLE, DBConstant.COLUMN_ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setBoolean(1, user.getLocked());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getMiddleName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getPasswordHash());
            statement.setString(6, user.getRole().toString());
            statement.setString(7, user.getId());
            statement.executeUpdate();
        }
    }

}