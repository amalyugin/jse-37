package ru.t1.malyugin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection(final boolean autoCommit) {
        @NotNull final String username = propertyService.getDatabaseUsername();
        @NotNull final String password = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(autoCommit);
        return connection;
    }

}