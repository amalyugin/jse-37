package ru.t1.malyugin.tm.exception.server;

import org.jetbrains.annotations.NotNull;

public final class UnknownRequestException extends AbstractServerException {

    public UnknownRequestException() {
        super("Error! Unknown request...");
    }

    public UnknownRequestException(@NotNull final String message) {
        super("Error! Unknown request: " + message);
    }
}