package ru.t1.malyugin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.service.IDomainService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

public final class DomainService implements IDomainService {

    @NotNull
    private static final String FILE_BASE64 = "data.base64";

    @NotNull
    private static final String FILE_BINARY = "data.bin";

    @NotNull
    private static final String FILE_XML = "data.xml";

    @NotNull
    private static final String FILE_JSON = "data.json";

    @NotNull
    private static final String FILE_YAML = "data.yaml";

    @NotNull
    private static final String FILE_BACKUP = "backup.bin";

    @NotNull
    private static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    private static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    private static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    private static final String APPLICATION_TYPE_JSON = "application/json";

    @NotNull
    private final String dumpDir;

    @NotNull
    private final IServiceLocator serviceLocator;

    public DomainService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.dumpDir = serviceLocator.getPropertyService().getApplicationDumpDir();
    }

    @NotNull
    private Domain getDomain() throws Exception {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    private void setDomain(@Nullable final Domain domain) throws Exception {
        if (domain == null) return;
        serviceLocator.getSessionService().clear();
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
    }

    @Override
    public boolean checkBackup() {
        @NotNull final String dumpPath = dumpDir + FILE_BACKUP;
        return (Files.exists(Paths.get(dumpPath)));
    }

    @Override
    @SneakyThrows
    public void loadDataBackup() {
        @NotNull final String dumpPath = dumpDir + FILE_BACKUP;
        @Cleanup @NotNull final FileInputStream fileInputStream = new FileInputStream(dumpPath);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBackup() {
        @NotNull final String dumpPath = dumpDir + FILE_BACKUP;
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(dumpPath);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataBase64() {
        @NotNull final String dumpPath = dumpDir + FILE_BASE64;
        @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(dumpPath));
        @Nullable final String base64Date = new String(base64Byte);
        @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Date);

        @Cleanup @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();

        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBase64() {
        @NotNull final String dumpPath = dumpDir + FILE_BASE64;
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(dumpPath);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void loadDataBinary() {
        @NotNull final String dumpPath = dumpDir + FILE_BINARY;
        @Cleanup @NotNull final FileInputStream fileInputStream = new FileInputStream(dumpPath);
        @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBinary() {
        @NotNull final String dumpPath = dumpDir + FILE_BINARY;
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(dumpPath);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataJsonFasterXml() {
        @NotNull final String dumpPath = dumpDir + FILE_JSON;
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(dumpPath));
        @NotNull final String json = new String(bytes);

        @NotNull final ObjectMapper objectMapper = new JsonMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);

        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataJsonFasterXml() {
        @NotNull final String dumpPath = dumpDir + FILE_JSON;
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(dumpPath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new JsonMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void loadDataJsonJaxB() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull final String dumpPath = dumpDir + FILE_JSON;
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);

        @NotNull final File file = new File(dumpPath);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);

        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataJsonJaxB() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull final String dumpPath = dumpDir + FILE_JSON;
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(dumpPath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void loadDataXmlFasterXml() {
        @NotNull final String dumpPath = dumpDir + FILE_XML;
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(dumpPath));
        @NotNull final String xml = new String(bytes);

        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);

        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataXmlFasterXml() {
        @NotNull final String dumpPath = dumpDir + FILE_XML;
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(dumpPath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void loadDataXmlJaxB() {
        @NotNull final String dumpPath = dumpDir + FILE_XML;
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        @NotNull final File file = new File(dumpPath);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);

        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataXmlJaxB() {
        @NotNull final String dumpPath = dumpDir + FILE_XML;
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(dumpPath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
    }

    @Override
    @SneakyThrows
    public void loadDataYamlFasterXml() {
        @NotNull final String dumpPath = dumpDir + FILE_YAML;
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(dumpPath));
        @NotNull final String yaml = new String(bytes);

        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);

        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataYamlFasterXml() {
        @NotNull final String dumpPath = dumpDir + FILE_YAML;
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(dumpPath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        fileOutputStream.write(yaml.getBytes());
        fileOutputStream.flush();
    }

}